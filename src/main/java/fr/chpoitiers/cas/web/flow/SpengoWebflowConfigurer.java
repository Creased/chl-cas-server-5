package fr.chpoitiers.cas.web.flow;

import org.apereo.cas.web.flow.AbstractCasWebflowConfigurer;
import org.apereo.cas.web.flow.CasWebflowConstants;
import org.springframework.webflow.definition.registry.FlowDefinitionRegistry;
import org.springframework.webflow.engine.ActionState;
import org.springframework.webflow.engine.Flow;
import org.springframework.webflow.engine.builder.support.FlowBuilderServices;

/**
 * The {@link SpengoWebflowConfigurer} is responsible for adjusting the CAS webflow context for spnego integration.
 *
 * @author Misagh Moayyed
 * @since 4.2
 */
public class SpengoWebflowConfigurer extends AbstractCasWebflowConfigurer {

    private static final String SPNEGO                    = "spnego";

    private static final String START_SPNEGO_AUTHENTICATE = "startSpnegoAuthenticate";

    private static final String SPNEGO_NEGOTIATE          = "negociateSpnego";

    private static final String EVALUATE_SPNEGO_CLIENT    = "evaluateClientRequest";

    /** Transition intermédiaire qui indique qu'on a envoyé un 401 */
    private static final String NEGOTIATE                 = "negotiate";

    /**
     * Vue envoyée en même temps que le 401 pour déclencher le SPNEGO, ne sera affiché que si le navigateur ne veut pas faire de
     * SPNEGO
     */
    private static final String NEGOTIATE_VIEW            = "casSpnegoNegotiateView";

    /**
     * Vue envoyée en cas d'erreur SPNEGO (pas à la négociation, mais après, par exemple si le token n'est pas valide
     */
    private static final String ERROR_VIEW                = "casSpnegoErrorView";

    /** Vue envoyée en cas de ticket invalide */
    private static final String AUTH_ERROR_VIEW           = "casSpnegoAuthenticationFailureView";

    public SpengoWebflowConfigurer(final FlowBuilderServices flowBuilderServices, final FlowDefinitionRegistry loginFlowDefinitionRegistry) {
        super(flowBuilderServices, loginFlowDefinitionRegistry);
    }

    @Override
    protected void doInitialize() throws Exception {

        final Flow flow = getLoginFlow();
        if (flow != null) {

            final ActionState actionState = createActionState(flow, START_SPNEGO_AUTHENTICATE,
                    createEvaluateAction(SPNEGO_NEGOTIATE));
            actionState.getTransitionSet().add(createTransition(CasWebflowConstants.TRANSITION_ID_SUCCESS, SPNEGO));
            actionState.getTransitionSet().add(createTransition(CasWebflowConstants.TRANSITION_ID_ERROR, ERROR_VIEW));
            actionState.getTransitionSet().add(createTransition(NEGOTIATE, NEGOTIATE_VIEW));

            final ActionState spnego = createActionState(flow, SPNEGO, createEvaluateAction(SPNEGO));
            spnego.getTransitionSet().add(createTransition(CasWebflowConstants.TRANSITION_ID_SUCCESS,
                    CasWebflowConstants.TRANSITION_ID_SEND_TICKET_GRANTING_TICKET));
            spnego.getTransitionSet().add(createTransition(CasWebflowConstants.TRANSITION_ID_ERROR, ERROR_VIEW));
            spnego.getTransitionSet()
                    .add(createTransition(CasWebflowConstants.TRANSITION_ID_AUTHENTICATION_FAILURE, AUTH_ERROR_VIEW));
            spnego.getExitActionList().add(createEvaluateAction("clearWebflowCredentialsAction"));

            createEndState(flow, NEGOTIATE_VIEW, NEGOTIATE_VIEW);
            createEndState(flow, ERROR_VIEW, ERROR_VIEW);
            createEndState(flow, AUTH_ERROR_VIEW, AUTH_ERROR_VIEW);

            registerMultifactorProvidersStateTransitionsIntoWebflow(spnego);

            final ActionState evaluateClientRequest = createActionState(flow, EVALUATE_SPNEGO_CLIENT,
                    createEvaluateAction(casProperties.getAuthn().getSpnego().getHostNameClientActionStrategy()));
            evaluateClientRequest.getTransitionSet()
                    .add(createTransition(CasWebflowConstants.TRANSITION_ID_YES, START_SPNEGO_AUTHENTICATE));
            evaluateClientRequest.getTransitionSet()
                    .add(createTransition(CasWebflowConstants.TRANSITION_ID_NO, getStartState(flow)));
        }
    }
}
