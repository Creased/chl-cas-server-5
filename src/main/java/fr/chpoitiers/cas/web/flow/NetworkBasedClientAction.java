package fr.chpoitiers.cas.web.flow;

import java.util.Collections;
import java.util.TreeMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apereo.cas.web.support.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.action.AbstractAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.chpoitiers.net.util.IpNetwork;
import fr.chpoitiers.net.util.IpNetworkFactory;

public class NetworkBasedClientAction extends AbstractAction {

    private final Logger                     log             = LoggerFactory.getLogger(NetworkBasedClientAction.class);

    /** Common header used behind an apache frontend */
    public final String                      X_FORWARDED_FOR = "x-Forwarded-For";

    /** Map between IP Networks and results. */
    private Map<IpNetwork, String> networkToResult = Collections.synchronizedSortedMap(new TreeMap<IpNetwork, String>());

    /** HTTP header containing client IP Address (null => request.getRemoteAddr() will be called instead */
    private String                           ipHeader;

    /**
     * Evaluate Client IP address:<br />
     * <ul>
     * <li>Client is in listed network : returns the corresponding literal transition provided</li>
     * <li>Client IP is valid but not found in list : returns success</li>
     * <li>Client IP not found or invalid : returns error</li>
     * </ul>
     */
    @Override
    protected Event doExecute(RequestContext context) throws Exception {

        final HttpServletRequest request = WebUtils.getHttpServletRequest(context);

        String clientIP;

        if (ipHeader == null) {
            clientIP = request.getRemoteAddr();
        } else {
            clientIP = request.getHeader(ipHeader);
        }

        if (clientIP != null) {
            IpNetwork ip = IpNetworkFactory.getFromString(clientIP);

            if (ip != null) {
                Set<IpNetwork> networks = networkToResult.keySet();
                for (IpNetwork network : networks) {
                    if (ip.isInNetwork(network)) {
                        if (log.isDebugEnabled()) {
                            log.debug("Read Ip from request : {} returning : {}", clientIP, networkToResult.get(network));
                        }
                        return result(networkToResult.get(network));
                    }
                }
                if (log.isDebugEnabled()) {
                    log.debug("Read Ip from request : {} returning success.", clientIP);
                }
                return success();
            }
        }

        log.debug("Unable to read Ip from request, returning error.");
        return error();
    }

    /**
     * Set the resulting string from source network
     * 
     * @param networkMap
     *            Map of Networks to result (string : CIDR network, string: literal result of transition)
     */
    public void setNetworkToResult(Map<String, String> networkMap) {

        if (!networkToResult.isEmpty()) {
            networkToResult.clear();
        }

        Set<String> rawNetworks = networkMap.keySet();
        for (String rawNetwork : rawNetworks) {

            IpNetwork network = IpNetworkFactory.getFromString(rawNetwork);
            if (network != null) {
                networkToResult.put(network, networkMap.get(rawNetwork));
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("populated networkToResult with : {}", networkToResult.toString());
        }
    }

    public String getIpHeader() {

        return ipHeader;
    }

    public void setIpHeader(String ipHeader) {

        this.ipHeader = ipHeader;
    }

}
