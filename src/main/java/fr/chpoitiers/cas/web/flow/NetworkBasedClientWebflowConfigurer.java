package fr.chpoitiers.cas.web.flow;

import java.util.Map;

import org.apereo.cas.web.flow.AbstractCasWebflowConfigurer;
import org.apereo.cas.web.flow.CasWebflowConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.definition.registry.FlowDefinitionRegistry;
import org.springframework.webflow.engine.ActionState;
import org.springframework.webflow.engine.Flow;
import org.springframework.webflow.engine.Transition;
import org.springframework.webflow.engine.TransitionSet;
import org.springframework.webflow.engine.builder.support.FlowBuilderServices;

public class NetworkBasedClientWebflowConfigurer extends AbstractCasWebflowConfigurer {

    /** Log */
    private static final Logger log                = LoggerFactory.getLogger(NetworkBasedClientWebflowConfigurer.class);

    /** Nom de l'état */
    public static final String  CLIENT_IP          = "clientIp";

    /** Action à évaluer */
    public static final String  EVALUATE_CLIENT_IP = "ipBasedDispatcher";

    /** Map de transition vers action */
    private Map<String, String> transitionsToActions;

    /** si aucune transition donnée, par défaut View Login Form */
    private String              defaultAction      = CasWebflowConstants.STATE_ID_VIEW_LOGIN_FORM;

    public NetworkBasedClientWebflowConfigurer(final FlowBuilderServices flowBuilderServices,
            final FlowDefinitionRegistry loginFlowDefinitionRegistry) {
        super(flowBuilderServices, loginFlowDefinitionRegistry);
    }

    @Override
    protected void doInitialize() throws Exception {

        final Flow flow = getLoginFlow();
        final ActionState actionState = createActionState(flow, CLIENT_IP, createEvaluateAction(EVALUATE_CLIENT_IP));
        final TransitionSet transitions = actionState.getTransitionSet();

        log.debug("Va ajouter les {} transitions configurées.", transitionsToActions.size());
        for (String transition : transitionsToActions.keySet()) {
            final String action = transitionsToActions.get(transition);
            Transition trans;
            if (action == null || action.isEmpty()) {
                log.debug("Crée la transition [{}] vers l'action par défaut [{}]", transition, defaultAction);
                trans = createTransition(transition, defaultAction);
            } else {
                log.debug("Crée la transition [{}] vers l'action [{}]", transition, action);
                trans = createTransition(transition, action);
            }
            transitions.add(trans);
        }

        /** Par défaut on ajoute les transitions error et success */
        transitions.add(createTransition(CasWebflowConstants.TRANSITION_ID_SUCCESS, defaultAction));
        transitions.add(createTransition(CasWebflowConstants.TRANSITION_ID_ERROR, defaultAction));
        log.debug("Ajout des transitions [{}],[{}] vers [{}]", CasWebflowConstants.TRANSITION_ID_SUCCESS,
                CasWebflowConstants.TRANSITION_ID_ERROR, defaultAction);
    }

    // Getters et Setters

    public Map<String, String> getTransitionsToActions() {

        return transitionsToActions;
    }

    public void setTransitionsToActions(Map<String, String> transitionsToActions) {

        this.transitionsToActions = transitionsToActions;
    }

    public String getDefaultTransition() {

        return defaultAction;
    }

    public void setDefaultTransition(String defaultTransition) {

        this.defaultAction = defaultTransition;
    }

}
