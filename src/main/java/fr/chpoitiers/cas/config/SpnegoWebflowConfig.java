package fr.chpoitiers.cas.config;

import org.apereo.cas.authentication.adaptive.AdaptiveAuthenticationPolicy;
import org.apereo.cas.configuration.CasConfigurationProperties;
import org.apereo.cas.web.flow.CasWebflowConfigurer;
import org.apereo.cas.web.flow.resolver.CasWebflowEventResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.webflow.definition.registry.FlowDefinitionRegistry;
import org.springframework.webflow.engine.builder.support.FlowBuilderServices;
import org.springframework.webflow.execution.Action;

import com.google.common.collect.Lists;

import fr.chpoitiers.cas.web.flow.SpengoWebflowConfigurer;
import fr.chpoitiers.cas.web.flow.SpnegoNegociateCredentialsAction;

@Configuration("chlSpnegoWebflowConfiguration")
@EnableConfigurationProperties(CasConfigurationProperties.class)
public class SpnegoWebflowConfig {

    /** Logger */
    public final Logger log = LoggerFactory.getLogger(SpnegoWebflowConfig.class);
    
    @Autowired
    @Qualifier("adaptiveAuthenticationPolicy")
    private AdaptiveAuthenticationPolicy adaptiveAuthenticationPolicy;

    @Autowired
    @Qualifier("serviceTicketRequestWebflowEventResolver")
    private CasWebflowEventResolver      serviceTicketRequestWebflowEventResolver;

    @Autowired
    @Qualifier("initialAuthenticationAttemptWebflowEventResolver")
    private CasWebflowEventResolver      initialAuthenticationAttemptWebflowEventResolver;

    @Autowired
    @Qualifier("loginFlowRegistry")
    private FlowDefinitionRegistry       loginFlowDefinitionRegistry;

    @Autowired
    private FlowBuilderServices          flowBuilderServices;

    @Autowired
    private CasConfigurationProperties   casProperties;

    @Bean(name = "spnegoWebflowConfigurer")
    public CasWebflowConfigurer spnegoWebflowConfigurer() {

        log.info("Instancie le configurateur du webflow SPNEGO");
        
        final SpengoWebflowConfigurer w = new SpengoWebflowConfigurer(flowBuilderServices, loginFlowDefinitionRegistry);
        return w;
    }

    @Bean
    @RefreshScope
    public Action negociateSpnego() {

        log.info("Instancie l'action SPNEGO Negociate Credentials");
        
        final String[] browsers = StringUtils
                .commaDelimitedListToStringArray(casProperties.getAuthn().getSpnego().getSupportedBrowsers());
        final SpnegoNegociateCredentialsAction a = new SpnegoNegociateCredentialsAction(Lists.newArrayList(browsers), 
                casProperties.getAuthn().getSpnego().isNtlm(), 
                casProperties.getAuthn().getSpnego().isMixedModeAuthentication());
        return a;
    }

}
