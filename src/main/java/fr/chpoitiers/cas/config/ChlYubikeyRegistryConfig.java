package fr.chpoitiers.cas.config;

import fr.chpoitiers.cas.support.mfa.yubikey.HardcodedYubikeyAccountRegistry;
import org.apereo.cas.adaptors.yubikey.YubiKeyAccountRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("yubikey")
public class ChlYubikeyRegistryConfig {

    /** Log */
    private static final Logger log = LoggerFactory.getLogger(ChlYubikeyRegistryConfig.class);

    @Bean(name = { "HardcodedYubikeyRegistry" })
    public static YubiKeyAccountRegistry hardcodedYubikeyRegistry() {

        log.info("Création du registre des comptes Yubikey");
        return new HardcodedYubikeyAccountRegistry();
    }
}
