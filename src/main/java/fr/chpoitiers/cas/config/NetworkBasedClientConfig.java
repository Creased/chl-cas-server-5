package fr.chpoitiers.cas.config;

import java.util.Map;

import org.apereo.cas.web.flow.CasWebflowConfigurer;
import org.apereo.cas.web.flow.CasWebflowConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.webflow.definition.registry.FlowDefinitionRegistry;
import org.springframework.webflow.engine.builder.support.FlowBuilderServices;

import fr.chpoitiers.cas.web.flow.NetworkBasedClientAction;
import fr.chpoitiers.cas.web.flow.NetworkBasedClientWebflowConfigurer;

@Configuration("networkBasedClientConfiguration")
@ConfigurationProperties("chl")
public class NetworkBasedClientConfig {

    /** Logger */
    private final static Logger log = LoggerFactory.getLogger(NetworkBasedClientConfig.class);

    /** Entête de la requête contenant l'adresse IP du client */
    private String              ipHeader;

    /** Sous réseau vers transition spring web flow, trié par réseau */
    private Map<String, String> networkMap;

    /** Transitions vers actions */
    private Map<String,String> transitionToAction;
    
    /** Transition par défaut */
    private String defaultAction = CasWebflowConstants.STATE_ID_VIEW_LOGIN_FORM;
    
    @Autowired
    @Qualifier("loginFlowRegistry")
    private FlowDefinitionRegistry loginFlowDefinitionRegistry;

    @Autowired
    private FlowBuilderServices flowBuilderServices;
    
    @Bean(name = "ipBasedDispatcher")
    @RefreshScope
    public NetworkBasedClientAction ipBasedDispatcher() {

        log.info("Va configurer NetworkBasedClientAction, ipHeader = {}, networkMap = {}", ipHeader, networkMap.toString());

        final NetworkBasedClientAction action = new NetworkBasedClientAction();

        if (ipHeader != null) {
            action.setIpHeader(ipHeader);
        }
        if (networkMap != null) {
            action.setNetworkToResult(networkMap);
        }
        return action;
    }

    @ConditionalOnMissingBean(name = "networkBasedClientWebflowConfigurer")
    @Bean
    public CasWebflowConfigurer networkBasedClientWebflowConfigurer() {
        final NetworkBasedClientWebflowConfigurer w = new NetworkBasedClientWebflowConfigurer(flowBuilderServices, loginFlowDefinitionRegistry);
        w.setTransitionsToActions(transitionToAction);
        w.setDefaultTransition(defaultAction);
        return w;
    }

    
    // Setters & Getters

    public String getIpHeader() {

        return ipHeader;
    }

    public void setIpHeader(String ipHeader) {

        this.ipHeader = ipHeader;
    }

    public Map<String, String> getNetworkMap() {

        return networkMap;
    }

    public void setNetworkMap(Map<String, String> networkMap) {

        this.networkMap = networkMap;
    }

        
    public Map<String, String> getTransitionToAction() {
    
        return transitionToAction;
    }

    
    public void setTransitionToAction(Map<String, String> transitionToAction) {
    
        this.transitionToAction = transitionToAction;
    }

}
