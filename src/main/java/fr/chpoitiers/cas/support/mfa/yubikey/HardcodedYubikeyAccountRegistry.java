package fr.chpoitiers.cas.support.mfa.yubikey;

import java.util.HashMap;
import javax.annotation.PostConstruct;
import org.apereo.cas.adaptors.yubikey.YubiKeyAccountRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HardcodedYubikeyAccountRegistry implements YubiKeyAccountRegistry {

    /** Logger */
    private static final Logger           log      = LoggerFactory.getLogger(HardcodedYubikeyAccountRegistry.class);
    
    /** Liste statique des clés */
    private final HashMap<String, String> yubikeys = new HashMap<String, String>();

    public HardcodedYubikeyAccountRegistry() {
        log.info("Peuple les yubikeys");
        this.yubikeys.put("mp081083", "cccccccccccb");
    }

    public boolean isYubiKeyRegisteredFor(String uid, String yubikeyPublicId) {

        String yubikey = (String) this.yubikeys.get(uid.toLowerCase());
        if (yubikey == null) {
            log.info("La clé [{}] n'est pas affectée à l'utilisateur {}", yubikeyPublicId, uid);
        } else {
            boolean match = yubikey.equalsIgnoreCase(yubikeyPublicId);
            if (match) {
                log.info("La yubikey [{}] est bien affectée à l'utilisateur {}", yubikeyPublicId, uid);
            } else {
                log.warn("La yubikey [{}] n'est pas affectée à l'utilisateur {}, clé attendue [{}]",
                        new Object[] { yubikeyPublicId, uid, yubikey });
            }
            return match;
        }
        return false;
    }

    @PostConstruct
    public void init() {

        log.info("Init terminée.");
    }
}
